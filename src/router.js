import Vue from 'vue'
import Router from 'vue-router'

// App Views
import Login from '@/views/login'
import Signup from '@/views/signup'
import Dashboard from '@/views/dashboard'

// Games Screens
import GamesList from '@/views/games/games-list'

// Account Settings Screen
import AccountSettings from '@/views/account'

Vue.use(Router)

// Route Configurations
export default new Router({
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    return savedPosition || {x: 0, y: 0}
  },
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup
    },
    {
      path: '/dashboard',
      alias: '/',
      name: 'dashboard',
      component: Dashboard,
      meta: {requiresAuth: true}
    },
    {
      path: '/games',
      name: 'games-list',
      component: GamesList,
      meta: {requiresAuth: true}
    },
    {
      path: '/account',
      name: 'account-settings',
      component: AccountSettings,
      meta: {requiresAuth: true}
    }
  ]
})
