import Vue from 'vue'
import Vuex from 'vuex'
import config from './config'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    firebase: config.firebaseConfig,
    observingAuthState: false,
    authenticated: false,
    user: null
  },
  mutations: {
    setState (state, props) {
      Object.keys(props).forEach(prop => {
        state[prop] = props[prop]
      })
    },
    login (state, user) {
      state.user = user
      state.authenticated = true
    }
  }
})
