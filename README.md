# dayo-ph

## Firebase Project Setup

1. `cd` into the project's directory and execute `firebase init`
1. Configure the project and make sure the main directory is `dist` instead of `public`

## File Structure

* All the codes are mainly inside `src` directory.
* `views` contains the modules of the app.
* `components` contains the custom components required by the views.
* `components/app-shell.vue` holds the main layout that contains the views when a user is authenticated.
* `main.js` is the entry point of all files. Authenticated routing algo is also in this file.
* `router.js` contains the routing configuration of the app.
* `state.js` contains the __firebase config, gameTypes labeling, and gameStatus labeling__.
* `theme.styl` contains the theme configuration.

## `.vue` files Code Structure

```html
<template>
  <main>
    <!-- markup contents -->
    The value of prop1 {{prop1}}
  </main>
</template>

<script>
  export default {
    name: 'module-name',
    // data used by the module or component
    data () {
      return {
        // properties and values that can be used by
        prop1: 'prop1 value'
      }
    },
    // methods holds the functions used within the module or component
    methods: {
      fn1 () {
        console.log('fn1 was called.')
      },
      fn2 () {
        console.log(`The value of prop1 is ${this.prop1}`)
      }
    },
    // this gets executed when the module or component has been created
    created () {
      console.log('The component has been successfully created.')
      console.log('Same as $(document).ready() but component wide only and not DOM wide.')
    }
  }
</script>

<style scoped>
  /* When 'scoped' attribute is present, styles will only be applied to this component. */
  /* Applying global styles is recommended to be written in a separate .css file and be required at main.js */
</style>

```

Main References:

* https://vuejs.org/v2/guide/single-file-components.html
* https://vuejs.org/v2/guide/instance.html
* https://vuejs.org/

## Design Framework used

* https://vuetifyjs.com/
* https://vuetifyjs.com/layout/pre-defined

## Development Process

> This required `vue-cli` to be globally installed. To do so, `npm install vue-cli -g`.

Reference: https://github.com/vuejs/vue-cli / https://github.com/vuejs-templates/webpack

* To run a development server, `cd` to the directory and type in `npm run dev`
* This will open a new browser window and will live reload with every changes in the code.

## Build Process

* Before deploying, you need to build the sources first.
* This will optimize the codes, concat everything that is required, etc.
* To do so, execute `npm run build` inside the project directory.
* This will create a directory `dist` that will contain all the build files ready for deployment.

## Deployment Process

* This requires the _Firebase Setup Project_ first to be done.
* Make sure that the main directory is __`dist`__ and __NOT `public`__.
